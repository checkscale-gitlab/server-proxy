FROM python:3.9-alpine

RUN pip3 install \
	flask \
	flask-cors

WORKDIR /srv/server-proxy
