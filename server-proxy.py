from flask import (
    Flask,
    Response,
    jsonify,
    make_response,
    request,
)
from flask_cors import CORS
from os import getenv
from pprint import pprint
from random import uniform as rand
from .log import log
from .cache import cache
from .delay import delay
from .pass_to_server import pass_to_server


app = Flask(__name__)
CORS(app)


REAL_SERVER_URL = getenv('REAL_SERVER_URL')

MAIN_HTTP_METHODS = [
    'HEAD',
    'GET',
    'POST',
    'PUT',
    'DELETE',
    'OPTIONS',
]

@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
# @delay()
# @delay(lambda: rand(0.0, 3.0))
# @cache(REAL_SERVER_URL)
# @log(REAL_SERVER_URL)
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
